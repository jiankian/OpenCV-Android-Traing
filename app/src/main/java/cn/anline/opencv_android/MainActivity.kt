package cn.anline.opencv_android

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import org.opencv.android.OpenCVLoader

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Example of a call to a native method
        sample_text.text = stringFromJNI()
//        调用初始化OpenCV方法
        initLoadOpenCV()
    }

    /**
     * 初始化OpenCV方法
     */
    fun initLoadOpenCV(){
        val isLoad = OpenCVLoader.initDebug()
        if (isLoad){
            Log.i("initLoadOpenCV", "OpenCv Library is loaded")
            tv_opencv_info.text = "OpenCv is loaded"
            tv_opencv_info.setTextColor(Color.GREEN)
        } else {
            Log.e("initLoadOpenCV", "OpenCv Library failed to load")
            tv_opencv_info.text = "OpenCv failed to load"
            tv_opencv_info.setTextColor(Color.RED)
        }
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String

    companion object {

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }
}
